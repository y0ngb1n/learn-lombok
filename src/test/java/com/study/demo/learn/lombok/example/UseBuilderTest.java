package com.study.demo.learn.lombok.example;

import org.junit.Test;

/**
 * @author yangbin
 **/
public class UseBuilderTest {

    @Test
    public void testUseBuilder() {

        // @Builder: 使用 builder 模式创建对象.

        UseBuilder example = UseBuilder.builder().id(1212L).name("杨斌").age(18).build();
    }
}