package com.study.demo.learn.lombok.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author yangbin
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class UseArgsConstructorTest {

    @Test
    public void testNoArgsConstructor() {

        // @NoArgsConstructor: 创建一个无参构造函数

        UseNoArgsConstructor example = new UseNoArgsConstructor();
    }

    @Test
    public void testAllArgsConstructor() {

        // @AllArgsConstructor: 创建一个全参构造函数.

        UseAllArgsConstructor example = new UseAllArgsConstructor(1212L, "杨斌");
    }

    @Test
    public void testRequiredArgsConstructor() {

        // @RequiredArgsConstructor: 注解用在类上, 使用类中所有带有 @NonNull 注解的或者带有 final 修饰的成员变量生成对应的构造方法.

        UseRequiredArgsConstructor example = new UseRequiredArgsConstructor("杨斌");
    }
}