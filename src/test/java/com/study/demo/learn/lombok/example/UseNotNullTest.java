package com.study.demo.learn.lombok.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author yangbin
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class UseNotNullTest {

    @Test
    public void testNotNull() {

        // @NotNull: 在参数中使用时, 如果调用时传了 null 值, 就会抛出空指针异常.

        UseNotNull example = new UseNotNull();

        // pass
        example.setId(null);

        // fail
        example.setName(null);
    }
}