package com.study.demo.learn.lombok.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author yangbin
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class UseSetterTest {

    @Test
    public void testSetter() {

        // @Setter：注解在类上, 为所有属性添加 set 方法、注解在属性上为该属性提供 set 方法.

        UseSetter example = new UseSetter();

        example.setId(1212L);
        example.setName("杨斌");
    }
}