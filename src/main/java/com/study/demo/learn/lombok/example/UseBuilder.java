package com.study.demo.learn.lombok.example;

import lombok.Builder;

/**
 * @author yangbin
 **/
@Builder
public class UseBuilder {

    private Long id;

    private String name;

    private Integer age;
}
