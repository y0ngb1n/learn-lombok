package com.study.demo.learn.lombok.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author yangbin
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class UseLoggerTest {

    @Test
    public void testLogger() {

        // @CommonsLog、@Log、@Log4j、@Log4j2、@Slf4j、@XSlf4j、@JBossLog 注解在类上, 为类提供一个属性名为 log 日志对象，提供默认构造方法.

        UseLogger.UseLog.log();
        UseLogger.UseLog4j2.log();
        UseLogger.UseSlf4j.log();
        UseLogger.UseCommonsLog.log();
    }
}